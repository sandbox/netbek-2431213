<?php

/**
 * @file
 * Time Zone
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Implements hook_field_info().
 */
function time_zone_field_info() {
  return array(
    'time_zone' => array(
      'label' => t('Time zone'),
      'description' => t('Stores a time zone.'),
      'default_widget' => 'options_select',
      'default_formatter' => 'time_zone_default',
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function time_zone_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $options = array_keys(system_time_zones());

  foreach ($items as $delta => $item) {
    if (!empty($item['value']) && !in_array($item['value'], $options)) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'time_zone_invalid',
        'message' => t('Time zone is not valid: %name', array('%name' => t($instance['label']))),
      );
    }
  }

  return $items;
}

/**
 * Implements hook_field_is_empty().
 */
function time_zone_field_is_empty($item, $field) {
  return empty($item['value']);
}

/**
 * Implements hook_field_formatter_info().
 */
function time_zone_field_formatter_info() {
  return array(
    'time_zone_default' => array(
      'label' => 'Time zone name',
      'field types' => array('time_zone'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function time_zone_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'time_zone_default':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item['value']);
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function time_zone_field_widget_info() {
  return array(
    'time_zone' => array(
      'label' => t('Time zone'),
      'field types' => array('time_zone'),
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function time_zone_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element += array(
    '#type' => 'select',
    '#options' => system_time_zones(),
    '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : variable_get('date_default_timezone', NULL),
  );
  return array('value' => $element);
}

/**
 * Implements hook_field_widget_error().
 */
function time_zone_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element['value'], $error['message']);
}
